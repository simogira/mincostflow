import os
import sys
import random

if len(sys.argv) != 3:
	print("generate ncompetences nagents")
	exit()
	
nc = int(sys.argv[1])
na = int(sys.argv[2])
val = 1.0/nc

with open("test.input", "w") as f:
	f.write("0.5\n")
	f.write(str(nc) + "\n")
	
	for i in range(nc): 
		f.write("C" + str(i) + " 1 " + str(val) +"\n")
	
	f.write(str(na) + "\n")
	for i in range(na): 
		f.write("A" + str(i) + " " + str(nc))
		for j in range(nc): 
			skill = round(random.uniform(0.2, 0.8), 2)
			f.write(" " + str(j) + " " + str(skill));
			
		f.write("\n")

	
"""
0.5
7
C0 1 0.142857
C1 1 0.142857
C2 1 0.142857
C3 1 0.142857
C4 1 0.142857
C5 1 0.142857
C6 1 0.142857
4
A3 7 0 0.65 1 0.7 2 0.35 3 0.55 4 0.75 5 0.85 6 0.8
A12 7 0 0.25 1 0.2 2 0.55 3 0.2 4 0.25 5 0.75 6 0.5
A13 7 0 0.8 1 0.65 2 0.5 3 0.55 4 0.75 5 0.85 6 0.65
A14 7 0 0.45 1 0.4 2 0.35 3 0.45 4 0.5 5 0.25 6 0.25
"""

