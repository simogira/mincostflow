// includes, system
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

// includes CUDA
#include <cuda_runtime.h>

#define CUDA_SAFE_CALL(call) { \
cudaError_t err = call; \
if (err != cudaSuccess) { \
printf("%s in %s at line %d\n", cudaGetErrorString(err), __FILE__, __LINE__); \
exit(err); \
} \
}

void printDeviceProperties(){
    int deviceCount;

    CUDA_SAFE_CALL(cudaGetDeviceCount(&deviceCount);)
    if (deviceCount == 0) {                                                  \
        fprintf(stderr, "There is no device supporting CUDA.\n");                            \
        exit(EXIT_FAILURE);                                                  \
    }

    printf("Number of available CUDA device:%3d\n", deviceCount);

    for (int dev =  0; dev < deviceCount; dev++) {
        cudaDeviceProp dev_prop;
        CUDA_SAFE_CALL(cudaGetDeviceProperties(&dev_prop, dev);)

        printf("Device Number:                                   %d\n", dev);
        printf("  Device name:                                   %s\n", dev_prop.name);
        printf("  Compute capability:                            %d.%d\n", dev_prop.major, dev_prop.minor);
        printf("  Total amount of global memory:                 %d bytes\n", deviceProp.totalGlobalMem);
        printf("  Total amount of constant memory:               %d bytes\n", deviceProp.totalConstMem);
        printf("  Total amount of shared memory per block:       %d bytes\n", deviceProp.sharedMemPerBlock);
        printf("  Memory Clock Rate (KHz):                       %d\n", dev_prop.memoryClockRate);
        printf("  Memory Bus Width (bits):                       %d\n", dev_prop.memoryBusWidth);
        printf("  Peak Memory Bandwidth (GB/s):                  %f\n", 2.0*dev_prop.memoryClockRate*(dev_prop.memoryBusWidth/8)/1.0e6);
        printf("  Max thread per block:                          %d\n", dev_prop.maxThreadsPerBlock);
    }
}

__global__
void minCostFlowDeviceKernel(){
    // TODO:
}

void minCostFlowDevice(){

    printDeviceProperties();
    std::cout << "Device solver is starting..." << '\n';

    // TODO:


}
