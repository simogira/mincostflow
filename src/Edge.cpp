#include "Edge.hpp"

Edge::Edge(Vertex* u, Vertex* v, int capacity, int residualFlow, int cost){
    this->u = u;
    this->v = v;
    this->capacity = capacity;
    this->residualFlow = residualFlow;
    this->cost = cost;
}
