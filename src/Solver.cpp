#include "Solver.hpp"
#include "Vertex.hpp"
#include "Edge.hpp"
#include "Graph.hpp"
#include "Utils.hpp"

#include <vector>
#include <climits>
#include <iostream>
#include <algorithm>
#include <fstream>
#include <string>
#include <sstream>

void Solver::solve(Graph* g, const char* testFilename){

  utils::log(LOG_INFO, "Solving the network...\n");

    int vertexCount = g->vertices.size();
    Vertex* source = g->vertices[0];
    Vertex* sink = g->vertices[vertexCount-1];

    int flow = 0;
    int requiredFlow = g->numCompetences;
    float minCost = 0;

    std::vector<Edge*> solution;
    // while there is a shortest path to the sink
    while(g->shortestPathBF(source, sink)) {
        std::vector<Edge*> path;
        // search minimum reidual flow of the path
        utils::log(LOG_DEBUG, "search minimum residual flow of the path\n");
        int pathMinFlow = requiredFlow - flow;
        Vertex* vertex = sink;
        while(vertex != source) {
            utils::log(LOG_DEBUG, "%s \n", vertex->name.c_str());
            Edge* e = vertex->parentEdge;
            utils::log(LOG_DEBUG, "---- min( %d, %d ): %d\n", pathMinFlow, e->residualFlow, std::min(pathMinFlow, e->residualFlow));
            pathMinFlow = std::min(pathMinFlow, e->residualFlow);
            vertex = e->u;

            path.push_back(e);
        }
        utils::log(LOG_DEBUG, "%s \n", vertex->name.c_str());
        utils::log(LOG_DEBUG, "pathMinFlow: %d\n", pathMinFlow);
        g->printEdges();

        // update residual flow of the path
        utils::log(LOG_DEBUG, "update residual flow");
        vertex = sink;

        minCost += pathMinFlow * sink->distance;

        while(vertex != source) {
            Edge* e = vertex->parentEdge;
            Edge* o = e->oppositeEdge;
            e->residualFlow -= pathMinFlow;
            o->residualFlow += pathMinFlow;
            vertex = e->u;

            utils::log(LOG_DEBUG, "residual flow ( %s, %s ): %d\n", e->u->name.c_str(), e->v->name.c_str(), e->residualFlow);
        }

        /* example
        S->A1 A1->C1 C1->T
        S->A1 A1->C1 C1->A2 A2->C2 C2->T
        S->A1 A1->C1 C1->A2 A2->C2 C2->A3 A3->C3 C3->T
        */
        // add even edges to the solution, remove odd edges
        std::reverse(path.begin(), path.end());
        for(int i = 1; i < path.size() - 1; i++){
            if (i % 2 == 1){
                solution.push_back(path[i]);
            }
            else{
                auto pos = std::find(solution.begin(), solution.end(), path[i]->oppositeEdge);
                if(pos != solution.end())
                    solution.erase(pos);
            }
        }

        flow += pathMinFlow;
        if(flow == requiredFlow)
            break;
    }

    if(flow != requiredFlow) {
        utils::log(LOG_WARNING, "CAN'T MATCH REQUIRED FLOW: %d != %d", flow, requiredFlow);
        return;
    }

    //genera assegnamento in output
    std::vector<std::string> testLines;
    std::ifstream fin;
    fin.open(testFilename);

    std::string line;
    int cmsaCost = 0;
    while(std::getline(fin, line))
    {
      testLines.push_back(line);
      //std::cout << line;

      for(Vertex* v : g->vertices){
          for(Edge* e : v->edges) {

                std::stringstream ss;
                ss << e->u->name << " " << e->v->name;
                std::string check  = ss.str();
                if(check.compare(line) == 0)
                {
                    //std::cout << " " << e->cost << " ";
                    cmsaCost += e->cost;
                }
            }
      }
      //std::cout << std::endl;

    }
    fin.close();


    bool lineError = false;

    //calculate assignment cost
    int assignmentCost = 0;
    for(Edge* e : solution)
        assignmentCost += e->cost;


    if(assignmentCost == cmsaCost)  {
      utils::log(LOG_INFO, "Success.\n");
    }
    else if(assignmentCost < cmsaCost){
          utils::log(LOG_INFO, "[OPTIMIZED] - assignment cost(%d) < cmsa cost(%d)\n", assignmentCost, cmsaCost);
    }
    else {
      utils::log(LOG_WARNING, "Assignment cost different\n");
      utils::log(LOG_WARNING, "------------- CMSA ( %d ) --------------\n", cmsaCost);
      for(auto line : testLines) {
          utils::log(LOG_WARNING, "%s\n", line.c_str());
      }
    }

    utils::log(LOG_INFO, "------------- Solution ( %d ) --------------\n", assignmentCost);
    for(auto e : solution) {
        utils::log(LOG_INFO, "%s %s\n", e->u->name.c_str(), e->v->name.c_str());
    }
    utils::log(LOG_INFO, "--------------------------------------------\n");
}
