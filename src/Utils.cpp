#include "Utils.hpp"


namespace utils {
  void log(log_level_t level, const char *msg, ...) {
    va_list args;
    va_start(args, msg);


    // GLOBAL_LEVEL is a global variable and could be changed at runtime
    // Any customization could be here
    if ( level <= GLOBAL_LEVEL ) {
        vprintf(msg, args);
    }

    va_end(args);
  }
}
