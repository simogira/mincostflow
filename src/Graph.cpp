#include "Graph.hpp"
#include "Vertex.hpp"
#include "Edge.hpp"
#include "Utils.hpp"

#include <math.h>
#include <limits.h>
#include <iostream>
#include <cstdio>
#include <string>
#include <fstream>
#include <cassert>
#include <algorithm>
#include <deque>

struct RequiredCompetence {
    std::string name;
    float level;
    float weight;
};

struct Agent {
    std::string name;
    std::vector<int> competenceIDs;
    std::vector<float> competenceLevels;
};

//calculateCost(level, competences[j].level, competences[j].weight, penality);
float calculateCost(float agentLevel, float requiredLevel, float w, float v) {
    assert((v >= 0 && v <= 1) && (w >= 0 && w <= 1));
    float levelDifference = agentLevel-requiredLevel;
    if (levelDifference < 0) {
        return -(levelDifference) * v * w;
    }
    else {
        return (levelDifference) * (1 - v) * w;
    }
}

void Graph::printEdges(){
    utils::log(LOG_DEBUG, "------------RESIDUAL-----------\n");
    for(Vertex* vertex: this->vertices) {
        for(Edge* edge: vertex->edges) {
          utils::log(LOG_DEBUG, "( %s, %s ) %d, %d\n", edge->u->name.c_str(), edge->v->name.c_str(), edge->residualFlow, edge->cost);
        }
    }
    utils::log(LOG_DEBUG, "-------------------------------\n");
}



//========================================
// Shortest path with Bellman-Ford
//========================================
bool Graph::shortestPathBF(Vertex* source, Vertex* sink) {
    int vertexCount = this->vertices.size();

    //===================================
    // Step 1: Initialize
    //===================================
    for(Vertex* vertex: this->vertices) {
        vertex->distance = 10000;
        vertex->parentEdge = nullptr;
    }
    source->distance = 0;

    //===================================
    //  // Step 2: relax edges repeatedly
    //===================================
    for(int i = 0; i < vertexCount - 1; i++) {
        for(Vertex* u: this->vertices) {
            for(Edge* edge: u->edges) {
                if(edge->residualFlow > 0) {
                    Vertex* v = edge->v;
                    if(u->distance + edge->cost < v->distance) {
                        v->distance = u->distance + edge->cost;
                        v->parentEdge = edge;
                    }
                }
            }
        }
    }

    //=========================================
    // Step 3: check for negative-weight cycles
    //=========================================
	for(Vertex* u: this->vertices){
		for(Edge* edge: u->edges){
			if(edge->residualFlow > 0){
				Vertex* v = edge->v;
				if(u->distance + edge->cost < v->distance){
                    utils::log(LOG_ERROR, "--------------------");
                    utils::log(LOG_ERROR, "ERROR NEGATIVE CYCLE");
                    utils::log(LOG_ERROR, "--------------------");
					return false;
				}
			}
		}
	}

    if(sink->parentEdge == nullptr)
        return false;   // sink is not reachable
    else
        return true;
}



//========================================
// Shortest path with D'Esopo-Pape
//========================================
bool Graph::shortestPathDP(Vertex* source, Vertex* sink){
    utils::log(LOG_DEBUG, "Calculating shortest path...\n");
    int vertexCount = this->vertices.size();

    //===================================
    // Initialize
    //===================================
    for(Vertex* vertex: this->vertices){
        vertex->distance = INT_MAX;
        vertex->parentEdge = nullptr;
        vertex->m = 2;
    }
    source->distance = 0;

    std::deque<Vertex*> q;
    q.push_back(source);

    //===================================
    //  Calculate
    //===================================
    while(!q.empty()){
        Vertex* u = q.front();
        q.pop_front();
        u->m = 0; //distance already calculated

        for(Edge* edge: u->edges){
            Vertex* v = edge->v;
            if(edge->residualFlow > 0 && v->distance > u->distance + edge->cost){
                v->distance = u->distance + edge->cost;
                v->parentEdge = edge;
                if(v->m == 2){ //distance has not yet been calculated
                    v->m = 1;  // processing distance
                    q.push_back(v);
                    utils::log(LOG_DEBUG, "NEW ( %s, %s): %d\n",u->name.c_str(), v->name.c_str(), edge->cost);
                }
                else if (v->m == 0){
                    v->m = 1;
                    q.push_front(v);
                    utils::log(LOG_DEBUG, "UPDATE ( %s, %s): %d\n",u->name.c_str(), v->name.c_str(), edge->cost);
                }
            }
        }
    }

    if(sink->parentEdge == nullptr) {
        return false;
    }
    else {
        return true;
    }
}

//======================================
// Generate .Dot file
//======================================
void Graph::generateDotFile() {
    utils::log(LOG_INFO, "Generating dot file...");
    std::ofstream fout("graph.dot");
    fout << "digraph G {\n";
    fout << "\trankdir=LR;\n";

    // for every child of the node print the node's name and the child's name
    for (Edge* e : this->edges) {
        fout << "\t" << e->u->name << " -> " << e->v->name <<  "[label = \""<< e->cost<< "\"];" << std::endl;
    }

    fout << "}\n";
    fout.close();

    utils::log(LOG_INFO, "Done.\n");
}

//======================================
// Convert problem instance to graph
//======================================
void Graph::loadFromFile(const char* filename) {
    utils::log(LOG_INFO, "Loading %s\n", filename);
    freopen(filename, "r", stdin);

    //============================
    // Read File
    //============================
    float penality;
    scanf("%f", &penality);
    utils::log(LOG_DEBUG, "penality: %f\n\n", penality);

    // read competence
    std::vector<RequiredCompetence> competences;
    int count;
    scanf("%d", &count);
    utils::log(LOG_DEBUG, "number of required competences: %d\n", count);
    this->numCompetences = count;
    for(int i = 0; i < count; i++) {
        struct RequiredCompetence rc;
        char s[64];
        scanf("%s %f %f", s, &rc.level, &rc.weight);
        rc.name = std::string(s);
        competences.push_back(rc);
    }

    utils::log(LOG_DEBUG, "NAME \t| LEVEL \t| WEIGHT\n");
    utils::log(LOG_DEBUG, "==================================\n");
    for(int i = 0; i < competences.size(); i++) {
        struct RequiredCompetence c = competences[i];
        utils::log(LOG_DEBUG, "%s \t| %f \t| %f \n", c.name.c_str(), c.level, c.weight);
    }
    utils::log(LOG_DEBUG, "\n");



    // read agents
    std::vector<Agent> agents;
    int competencesCount, competenceID;
    float competenceLevel;
    scanf("%d", &count); // in this case count is the number of agents
    utils::log(LOG_DEBUG, "number of involved agents: %d\n", count);
    for(int i = 0; i < count; i++) {
        struct Agent a;
        char s[64];
        scanf("%s %d", s, &competencesCount);
        a.name = std::string(s);
        utils::log(LOG_DEBUG, "Competences for agent %s:\n",a.name.c_str());
        for(int j = 0; j < competencesCount; j++) {
            scanf("%d %f", &competenceID, &competenceLevel);
            utils::log(LOG_DEBUG, " - %s with level %f\n", competences[competenceID].name.c_str(), competenceLevel);
            a.competenceIDs.push_back(competenceID);
            a.competenceLevels.push_back(competenceLevel);
        }
        agents.push_back(a);
    }



    //============================
    // Generate graph
    //============================
    utils::log(LOG_INFO, "Generating the network...\n");
    Vertex* source = new Vertex("S");
    this->vertices.push_back(source);

    //generate agent vertices
    utils::log(LOG_DEBUG, "generating agent vertices...");
    std::vector<Vertex*> agentsVertices;
    for (Agent a: agents) {
        Vertex* v = new Vertex(a.name);
        this->vertices.push_back(v);
        agentsVertices.push_back(v);
    }
    utils::log(LOG_DEBUG, "Done.\n");

    //generate competence vertices
    utils::log(LOG_DEBUG, "generating competence vertices...");
    std::vector<Vertex*> competencesVertices;
    for (RequiredCompetence c: competences) {
        Vertex* v = new Vertex(c.name);
        this->vertices.push_back(v);
        competencesVertices.push_back(v);
    }
    utils::log(LOG_DEBUG, "Done.\n");

    Vertex* sink = new Vertex("W");
    this->vertices.push_back(sink);


    int capacityOfSupplyEdges = ceil(competences.size() / (float) agents.size());
    utils::log(LOG_DEBUG, "capacity of supply edges: %d\n", capacityOfSupplyEdges);

    // generate supply edges
    utils::log(LOG_DEBUG, "generating supply edges...");
    for(Vertex* a : agentsVertices){
        Edge* e1 = new Edge(source, a, capacityOfSupplyEdges, capacityOfSupplyEdges, 0);
        Edge* e2 = new Edge(a, source, capacityOfSupplyEdges, 0, 0);
        e1->oppositeEdge = e2;
        e2->oppositeEdge = e1;

        source->edges.push_back(e1);
        a->edges.push_back(e2);
        edges.push_back(e1);
    }
    utils::log(LOG_DEBUG, "Done.\n");


    //generate transportation edges
    utils::log(LOG_DEBUG, "generating transportation edges...");
    for(int i = 0; i < agents.size(); i++) {
        //std::cout << "iteration " << i  << std::endl;
        Agent a = agents[i];
        Vertex* u = agentsVertices[i];
        for(int j = 0; j < a.competenceIDs.size(); j++) {

            //std::cout << "Agent[" << a.name << "] -- sub iteration " << j  << std::endl;
            int id = a.competenceIDs[j];
            float level = a.competenceLevels[j];
            Vertex* v = competencesVertices[id];
            //std::cout << "competence[" << v->name << "] of "<< competencesVertices.size() << std::endl;
            int precision = 1000;
            int transportCost = (int)(precision * calculateCost(level, competences[id].level, competences[id].weight, penality));

            //std::cout << "transportCost: " << transportCost << '\n';
            //std::cout << "  " << level << "," << competences[id].level << "(" << u->name << "," << v->name << "): "  << "transportCost: " << transportCost << std::endl;
            Edge* e1 = new Edge(u, v, 1, 1, transportCost);
            Edge* e2 = new Edge(v, u, 1, 0, -transportCost);
            e1->oppositeEdge = e2;
            e2->oppositeEdge = e1;

            u->edges.push_back(e1);
            v->edges.push_back(e2);
            edges.push_back(e1);
        }
    }
    utils::log(LOG_DEBUG, "Done.\n");


    // generate demand edges
    utils::log(LOG_DEBUG, "generating demand edges...");

    for(Vertex* v : competencesVertices){
        Edge* e1 = new Edge(v, sink, 1, 1, 0);
        Edge* e2 = new Edge(sink, v, 1, 0, 0);
        e1->oppositeEdge = e2;
        e2->oppositeEdge = e1;

        v->edges.push_back(e1);
        sink->edges.push_back(e2);
        edges.push_back(e1);
    }
    utils::log(LOG_DEBUG, "Done.\n");


    //std::cout << "Load complete." << std::endl;
    //std::cout << "-------------------------------------" << std::endl;
}
