#pragma once

class Graph;

class Solver {
public:
	/** @brief Calculates the mincostflow of the given graph
	 *  @param graph The angents assignment problem graph
	 *  @param testFilename The cmsa test filename
	 *  @return void
	 */
    void solve(Graph* graph, const char* testFilename);
};
