#pragma once
#include <vector>

class Edge;
class Vertex;

class Graph {
public:
    int numCompetences;
    std::vector<Edge*> edges;
    std::vector<Vertex*> vertices;
	
	/** @brief Load agent assignment problem file
	 *  @param Converts the agent assignment problem (e.g. the given input file) to a graph
	 *  @return void
	 */
    void loadFromFile(const char* filename);
	
	/** @brief Generate a .dot (graphviz graph file) http://www.webgraphviz.com/
	 *  @return void
	 */
    void generateDotFile();
	
	/** @brief Print all the graph edges on the console
	 *  @return void
	 */
    void printEdges();
	
	/** @brief Calculate the shortest path from source to sink updating all the
	 *		   vertices distances using D'Esopo-Pape Algorithm
	 *  @param from The starting vertex
	 *  @param to The vertex to reach
	 *  @return true if a path exists
	 */
    bool shortestPathDP(Vertex* source, Vertex* sink);
	
	/** @brief Calculate the shortest path from source to sink updating all the 
	 *		   vertices distances using Bellman-Ford algorithm
	 *  @param from The starting vertex
	 *  @param to The vertex to reach
	 *  @return true if a path exists
	 */
    bool shortestPathBF(Vertex* source, Vertex* sink);
};


