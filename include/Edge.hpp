#pragma once

class Vertex;

class Edge
{
    public:
        Edge* oppositeEdge;
        Vertex* u;
        Vertex* v;
        int capacity;
        int residualFlow;
        int cost;
        Edge(Vertex* u, Vertex* v, int capacity, int residualFlow, int cost);
        ~Edge();
};
