#pragma once


#include <stdarg.h>     /* va_list, va_start, va_arg, va_end */
#include <sstream>


#define GLOBAL_LEVEL LOG_INFO

enum log_level_t {
    LOG_NOTHING,
    LOG_CRITICAL,
    LOG_ERROR,
    LOG_WARNING,
    LOG_INFO,
    LOG_DEBUG
};


namespace utils {
	/** @brief simple log function
	 *  @param level the log level
	 *  @param similar use to printf
	 *  @return void 
	 */
	void log(log_level_t level, const char *msg, ...);
}
