
#include <string>
#include <vector>

class Edge;

class Vertex {
    public:
        std::string name;
        std::vector<Edge*> edges;
        int distance;
        Edge* parentEdge;

        // m=0 -> distance has already been calculated (although it might not be the final distance)
        // m=1 -> distance is being calculated
        // m=2 -> distance has not yet been calculated
        int m; //used by D'Esopo-Pape algorithm
		
        Vertex(std::string name);
        ~Vertex();
};
