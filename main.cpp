#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <chrono>  // for high_resolution_clock

#include "include/Graph.hpp"
#include "include/Solver.hpp"

bool fileExists (const char* name) {
    std::ifstream f(name);
    return f.good();
}

//////////////////////////////////////////////////////////////
// MAIN
//////////////////////////////////////////////////////////////
int main(int argc, char const *argv[]){

    std::ofstream ofs("time_measurement.txt", std::ofstream::app);

    if (!(argc == 2 || argc == 3)) {
        std::cout << "usage: minCostFlow <input_file>" << "\n";
        exit(EXIT_FAILURE);
    }

    Graph g;
    if(!fileExists(argv[1])) {
        std::cout << "INPUT FILE DOES NOT EXISTS" << '\n';
        exit(EXIT_FAILURE);
    }

    // load network from the input file
    g.loadFromFile(argv[1]);

    // generate dot file of loaded network
    g.generateDotFile();

    Solver* solver = new Solver();

    // Record start time in seconds
    auto start = std::chrono::high_resolution_clock::now();

    solver->solve(&g, argv[2]);     // se teniamo argv[2] bvisogna fare il check argc etc etc...

    // Record end time
    auto finish = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = finish - start;
    ofs << elapsed.count() << '\n';
    ofs.close();

    return 0;
}
