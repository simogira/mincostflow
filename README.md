# mincostflow

AI UniVR Project 2018

### How to build and run:
In build folder there are both "build_linux.sh" and "build_windows.bat" for Unix and MS-DOS systems respectively. You can use these files to compile the source code for your platform and if the compilation ends successfully, you can run the relative "test" file.

### Input File Description:
    <penality>
    <number of competences>
    <competence name> <competence level required> <importance of the competence>
    <number of agents>
    <agent name> <number of competences for this agent> <competence id> <level of competence>

*⚠️ At the moment the program take an input file and an output file (see test files) to compare the results with the output of Cplex mincostflow program and verify the correctness.*
